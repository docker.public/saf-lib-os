FROM ubuntu:trusty
MAINTAINER Miguel Neyra <mneyra@americatel.com.pe>

RUN apt-get update -y && apt-get upgrade -y \
		&& apt-get install -y libssl-dev \
		&& apt-get install -y curl \
		&& apt-get install -y openssh-client \
		&& apt-get install -y unzip \
		&& apt-get install -y make \
		&& apt-get install -y gcc \
		&& apt-get install -y zlib1g-dev \
		&& apt-get install -y libmysqlclient-dev \
		&& cd /lib && ln -s x86_64-linux-gnu/libz.so.1.2.8 libz.so && ln -s x86_64-linux-gnu/libz.so.1.2.8 x86_64-linux-gnu/libz.so \
		&& cd /lib && ln -s x86_64-linux-gnu/libssl.so.1.0.0 libssl.so && ln -s x86_64-linux-gnu/libssl.so.1.0.0 x86_64-linux-gnu/libssl.so